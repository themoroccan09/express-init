/**
 * Created by themoroccan09 on 29/09/17.
 */
var express = require('express');
var http = require('http');
var app = express();
var fs = require('fs');
var path = require('path');
var jwt = require('jsonwebtoken');
var jsonPath = path.join(__dirname, './ssl/private.pem');
var jsonPath2 = path.join(__dirname, './ssl/private.pem');
var cert = fs.readFileSync(jsonPath);
var pubKey = fs.readFileSync(jsonPath2);
var token = jwt.sign({first_name: 'Othmane'}, cert);


// Set headers
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});
app.get('/', (req, res) => {
    if(token) {
        jwt.verify(token, pubKey, function(err, decode) {
            res.end(decode.first_name);
        });
    }else {
        res.end('Not token yet');
    }
});

var server = http.createServer(app);
server.listen(3000);
